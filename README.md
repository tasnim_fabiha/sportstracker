# README #

This project was my 5th semester Software Project Lab-2(SPL-2), where I along with my partner implemented a simple sport-tracker website.
It could collect lat-long from files and show graphical representation, estimated burnt calorie, etc.
It had also a social feature where my partner implemented individual user profiles, messaging and sharing options. 

This project was previously up as a hg repository, which got deleted recently.
https://bitbucket.org/blog/sunsetting-mercurial-support-in-bitbucket

I communicated with the team to recover the repo and they helped me. Hence I am re-uploading the whole project as git here.
https://community.atlassian.com/t5/Bitbucket-questions/Unable-to-find-my-repository/qaq-p/1773276?utm_source=atlcomm&utm_medium=email&utm_campaign=immediate_general_reply&utm_content=topic

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact